<?php

namespace FpDbTest;

use Exception;
use FpDbTest\Compiler\Ast\BlackHole;
use FpDbTest\Compiler\Ast\Query;
use mysqli;

class Database implements DatabaseInterface
{
    private mysqli $mysqli;

    //    public function __construct(mysqli $mysqli)
    //    {
    //        $this->mysqli = $mysqli;
    //    }

    /**
     * @throws Exception
     */
    public function buildQuery(string $query, array $args = []): string
    {


        $q = new Query($query, $args);
        return $q->compile();
    }

    public function skip(): BlackHole
    {
        return new BlackHole();
    }


}
