<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

class SharpPositionalParser extends QuestionMarkSharpParser
{
    public function pattern(): string
    {
        return '#(?=(\s|$))';
    }
}
