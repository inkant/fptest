<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Parsers\Traits\DefaultCompositeParserImplementationTrait;

class QueryParser extends CompositeParserAbstract
{
    use DefaultCompositeParserImplementationTrait;

    public function pattern(): string
    {
        return '.*';
    }
}
