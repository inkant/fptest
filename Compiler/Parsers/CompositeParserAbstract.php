<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

abstract class CompositeParserAbstract extends ParserAbstract
{
    /**
     * @return ParserAbstract[]
     */
    abstract public function parsers(): array;

}
