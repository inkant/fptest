<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Ast\Literal;
use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Contracts\ParserInterface;
use FpDbTest\Compiler\Parsers\Traits\NamedParameterParserTrait;

class QuestionMarkNamedParser implements ParserInterface
{
    use NamedParameterParserTrait;

    public function __construct(
        protected array $named,
        protected array &$ordered
    ) {

    }

    public function pattern(): string
    {
        return '\?[a-zA-Z0-9_]+';
    }

    protected function extractName(string $raw): string
    {
        return substr($raw, 1);
    }

    protected function component(mixed $value): AstNodeInterface
    {
        return new Literal($value);
    }
}
