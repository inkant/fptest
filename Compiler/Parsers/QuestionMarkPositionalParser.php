<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Ast\Literal;
use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Contracts\ParserInterface;
use FpDbTest\Compiler\Parsers\Traits\PositionalParamParserTrait;

class QuestionMarkPositionalParser implements ParserInterface
{
    use PositionalParamParserTrait;

    public function __construct(
        protected array $named,
        protected array &$ordered
    ) {

    }

    public function pattern(): string
    {
        return '\?(?=(\s|$))';
    }

    protected function component(mixed $value): AstNodeInterface
    {
        return new Literal($value);
    }
}
