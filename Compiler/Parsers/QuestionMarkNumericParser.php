<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Ast\NullLiteral;
use FpDbTest\Compiler\Ast\NumericLiteral;
use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Contracts\ParserInterface;
use FpDbTest\Compiler\Exceptions\ReservedParameterNameException;
use FpDbTest\Compiler\Parsers\Traits\PositionalParamParserTrait;

class QuestionMarkNumericParser implements ParserInterface
{
    use PositionalParamParserTrait;
    public function __construct(
        protected array $named,
        protected array &$ordered
    ) {
        if (array_key_exists('f', $this->named)) {
            throw new ReservedParameterNameException('f');
        }
    }

    public function pattern(): string
    {
        return '\?f(?=(\s|$))';
    }

    protected function component(mixed $value): AstNodeInterface|null
    {
        if ($value === null) {
            return new NullLiteral();
        }

        if (!is_numeric($value)) {
            throw new \Exception('value is not numeric');
        }

        $value = (float) $value;

        return new NumericLiteral($value);
    }
}
