<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Ast\Identifier;
use FpDbTest\Compiler\Ast\Identifiers;
use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Contracts\ParserInterface;
use FpDbTest\Compiler\Exceptions\WrongIdentifier;
use FpDbTest\Compiler\Parsers\Traits\PositionalParamParserTrait;

class QuestionMarkSharpParser implements ParserInterface
{
    use PositionalParamParserTrait;

    public function __construct(
        protected array $named,
        protected array &$ordered
    ) {

    }

    public function pattern(): string
    {
        return '\?#(?=(\s|$))';
    }

    protected function component(mixed $value): AstNodeInterface
    {
        if (!is_array($value) && !is_string($value)) {
            throw new WrongIdentifier();
        }

        if (is_string($value)) {
            return new Identifier($value);
        }

        foreach ($value as $v) {
            if (!is_string($v)) {
                throw new WrongIdentifier();
            }
        }

        return new Identifiers(...$value);
    }
}
