<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Contracts\ParserInterface;

abstract class ParserAbstract implements ParserInterface
{
    abstract public function pattern(): string;
}
