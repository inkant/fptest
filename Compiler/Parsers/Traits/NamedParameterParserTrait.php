<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers\Traits;

use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Exceptions\NamedParameterNotFoundException;

trait NamedParameterParserTrait
{
    protected function getNamedParameter(string $name): mixed
    {
        if (!array_key_exists($name, $this->named)) {
            throw new NamedParameterNotFoundException($name);
        }
        return $this->named[$name];
    }

    public function parse(mixed $raw): AstNodeInterface
    {
        $name  = $this->extractName($raw);
        $value = $this->getNamedParameter($name);
        if ($value instanceof AstNodeInterface) {
            return $value;
        }
        return $this->component($value);
    }
}
