<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers\Traits;

use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Exceptions\PositionalParameterNotFoundException;

trait PositionalParamParserTrait
{
    protected function getNextPositionalParameter(): mixed
    {
        if (key($this->ordered) === null) {
            $count = count($this->ordered);
            throw new  PositionalParameterNotFoundException($count + 1);
        }
        $value = current($this->ordered);
        next($this->ordered);
        return $value;
    }

    public function parse(mixed $raw): AstNodeInterface
    {
        $value = $this->getNextPositionalParameter();
        if ($value instanceof AstNodeInterface) {
            return $value;
        }
        return $this->component($value);
    }

}
