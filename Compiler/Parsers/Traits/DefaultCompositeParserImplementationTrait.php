<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers\Traits;

use FpDbTest\Compiler\Ast\AstNode;
use FpDbTest\Compiler\Ast\Composite;
use FpDbTest\Compiler\Contracts\ParserInterface;
use FpDbTest\Compiler\Exceptions\QueryParserException;

trait DefaultCompositeParserImplementationTrait
{
    protected array $parsers;
    public function __construct(
        ParserInterface ...$parsers
    ) {
        $this->parsers = $parsers;
    }

    public function parsers(): array
    {
        return $this->parsers;
    }

    protected function patterns(): string
    {
        $patterns = [];
        foreach ($this->parsers as $replacer) {
            $patterns[] = '(' . $replacer->pattern() . ')';
        }
        return implode('|', $patterns);
    }

    public function parse(string $raw): Composite
    {
        preg_match_all(
            '~' . $this->patterns() . '~i',
            $raw,
            $matches,
            PREG_OFFSET_CAPTURE
        );

        $components = [];
        $prevOffset = 0;
        foreach ($matches[0] ?? [] as [$substr, $offset]) {
            foreach ($this->parsers as $parser) {

                $pattern = '~^' . $parser->pattern() . '$~i';
                if (preg_match($pattern, $substr) !== 1) {
                    continue;
                }

                $components[] = new AstNode(substr(
                    $raw,
                    $prevOffset,
                    $offset - $prevOffset
                ));
                $prevOffset = $offset + strlen($substr);

                try {
                    $components[] = $parser->parse($substr);
                } catch (\Exception $e) {
                    throw new QueryParserException($raw, $offset, $e);
                }
                break;
            }
        }

        $components[] = new AstNode(substr(
            $raw,
            $prevOffset
        ));

        return new Composite(...$components);
    }
}
