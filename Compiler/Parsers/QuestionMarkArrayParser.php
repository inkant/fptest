<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Ast\KeyValue;
use FpDbTest\Compiler\Ast\Literals;
use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Contracts\ParserInterface;
use FpDbTest\Compiler\Exceptions\ReservedParameterNameException;
use FpDbTest\Compiler\Exceptions\ValueMustBeArray;
use FpDbTest\Compiler\Parsers\Traits\PositionalParamParserTrait;

class QuestionMarkArrayParser implements ParserInterface
{
    use PositionalParamParserTrait;
    public function __construct(
        protected array $named,
        protected array &$ordered
    ) {
        if (array_key_exists('a', $this->named)) {
            throw new ReservedParameterNameException('a');
        }
    }

    public function pattern(): string
    {
        return '\?a(?=(\s|$))';
    }

    protected function component(mixed $value): AstNodeInterface
    {
        if (!is_array($value)) {
            throw new ValueMustBeArray();
        }

        return array_is_list($value)
            ? new Literals(...$value)
            : new KeyValue($value);
    }
}
