<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Ast\NullLiteral;
use FpDbTest\Compiler\Ast\NumericLiteral;
use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Contracts\ParserInterface;
use FpDbTest\Compiler\Exceptions\ReservedParameterNameException;
use FpDbTest\Compiler\Parsers\Traits\PositionalParamParserTrait;

class QuestionMarkDigitsParser implements ParserInterface
{
    use PositionalParamParserTrait;

    public function __construct(
        protected array $named,
        protected array &$ordered
    ) {
        if (array_key_exists('d', $this->named)) {
            throw new ReservedParameterNameException('d');
        }
    }

    public function pattern(): string
    {
        return '\?d(?=(\s|$))';
    }

    protected function component(mixed $value): AstNodeInterface
    {
        if ($value === null) {
            return new NullLiteral();
        }

        if(is_bool($value)) {
            $value = (int) $value;
        }

        if (!is_numeric($value)) {
            throw new \Exception('"' . $value . '" is not numeric');
        }

        if (is_float($value)) {
            throw new \Exception('"' . $value . '" is not integer');
        }

        if (str_contains((string )$value, '.')) {
            throw new \Exception('"' . $value . '" is not integer');
        }

        $value = (int) $value;

        return new NumericLiteral($value);
    }
}
