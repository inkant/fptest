<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Ast\Identifier;
use FpDbTest\Compiler\Ast\Identifiers;
use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Parsers\Traits\PositionalParamParserTrait;

class SharpParser extends QuestionMarkSharpParser
{
    use PositionalParamParserTrait;

    public function pattern(): string
    {
        return '#(?=(\s|$))';
    }

    protected function component(array|string $value): AstNodeInterface
    {
        if (is_array($value)) {
            return new Identifiers(...$value);
        }

        return new Identifier($value);
    }
}
