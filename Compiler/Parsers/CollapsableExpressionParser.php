<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Parsers;

use FpDbTest\Compiler\Ast\CollapsableSubExpression;
use FpDbTest\Compiler\Ast\Composite;
use FpDbTest\Compiler\Exceptions\QueryParserException;

class CollapsableExpressionParser extends QueryParser
{
    public function pattern(): string
    {
        return '\{(.*?)\}';
    }

    /**
     * @throws QueryParserException
     */
    public function parse(string $raw): Composite
    {
        $template = substr($raw, 1, -1);
        return new CollapsableSubExpression(parent::parse($template));
    }
}
