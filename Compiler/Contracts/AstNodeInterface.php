<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Contracts;

interface AstNodeInterface
{
    public function compile(): string;
}
