<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Contracts;

interface ParserInterface
{
    public function parse(string $raw): AstNodeInterface;
}
