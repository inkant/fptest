<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class NullLiteral extends AstNodeAbstract
{
    public function compile(): string
    {
        return 'NULL';
    }
}
