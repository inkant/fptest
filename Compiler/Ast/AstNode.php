<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class AstNode extends AstNodeAbstract
{
    public function __construct(
        protected string $body
    ) {

    }

    public function compile(): string
    {
        return $this->body;
    }
}
