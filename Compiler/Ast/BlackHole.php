<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class BlackHole extends EmptyAstNode implements \JsonSerializable
{
    public function jsonSerialize(): mixed
    {
        return '$BLACK-HOLE$';
    }
}
