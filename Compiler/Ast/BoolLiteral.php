<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class BoolLiteral extends AstNodeAbstract
{
    public function __construct(
        protected bool|int $value
    ) {

    }

    public function compile(): string
    {
        return (string) (bool) $this->value;
    }
}
