<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class StringLiteral extends AstNodeAbstract
{
    public function __construct(
        protected string $value
    ) {

    }

    public function compile(): string
    {
        return "'" . str_replace("'", "''", $this->value) . "'";
    }
}
