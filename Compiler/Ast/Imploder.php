<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Contracts\AstNodeInterface;

class Imploder extends CompositeAstNodeAbstract
{
    protected array $items = [];
    public function __construct(
        protected string|AstNodeInterface $delimiter,
        string|AstNodeInterface ...$items
    ) {
        $this->items = $items;
    }

    protected function components(): array
    {
        $result = [];
        $delimiter =
            is_string($this->delimiter)
            ? new AstNode($this->delimiter)
            : $this->delimiter;

        foreach ($this->items as $k => $v) {
            if ($k !== 0) {
                $result[] = $delimiter;
            }
            $result[] =
                is_string($v)
                ? new AstNode($v)
                : $v;
        }
        return $result;
    }
}
