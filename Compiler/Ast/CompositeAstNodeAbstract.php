<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Exceptions\CompilerException;
use Traversable;

abstract class CompositeAstNodeAbstract extends AstNodeAbstract implements \IteratorAggregate
{
    protected Traversable $iterator;

    abstract protected function components(): iterable;

    public function getIterator(): Traversable
    {
        $components = $this->components();
        return $this->iterator ??=
            $components instanceof Traversable
            ? $components
            : new \ArrayIterator($components);
    }

    final public function compile(): string
    {
        $result = '';
        foreach ($this as $component) {
            $result .= $component->compile();
        }
        return $result;
    }

    public function contains(string $class): ?AstNodeInterface
    {
        if (is_a($this, $class, true)) {
            return $this;
        }

        foreach ($this as $component) {
            if ($object = $component->contains($class)) {
                return $object;
            }
        }
        return null;
    }
}
