<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class Identifier extends AstNodeAbstract
{
    public function __construct(
        protected string $identifier
    ) {

    }

    public function compile(): string
    {
        return '`'. str_replace('`', '``', $this->identifier) .  '`';
    }
}
