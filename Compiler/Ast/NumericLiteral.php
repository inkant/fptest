<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class NumericLiteral extends AstNodeAbstract
{
    public function __construct(
        protected float|int $value
    ) {

    }

    public function compile(): string
    {
        return (string) $this->value;
    }
}
