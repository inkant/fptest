<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Exceptions\LiteralsListCanNotBeEmpty;

class Literals extends CompositeAstNodeAbstract
{
    protected array $items;
    public function __construct(
        mixed ...$items
    ) {
        if (empty($items)) {
            throw new LiteralsListCanNotBeEmpty();
        }

        foreach ($items as $item) {
            $this->items[] =
                $item instanceof AstNodeInterface
                    ? $item
                    : new Literal($item);
        }

    }

    protected function components(): iterable
    {
        return new Imploder(', ', ...$this->items);
    }
}
