<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Exceptions\ArrayEmtpyException;
use FpDbTest\Compiler\Exceptions\KeyValueBadKeyException;

class KeyValue extends CompositeAstNodeAbstract
{
    public function __construct(
        protected array $assoc
    ) {
        if (empty($this->assoc)) {
            throw new ArrayEmtpyException();
        }
        foreach ($this->assoc as $k => $v) {
            if (!is_string($k)) {
                throw new KeyValueBadKeyException();
            }
        }
    }

    protected function components(): iterable
    {
        $result = [];
        foreach ($this->assoc as $k => $v) {
            $result[] = new Imploder(
                ' = ',
                new Identifier($k),
                new Literal($v)
            );
        }
        return new Imploder(", ", ...$result);
    }
}
