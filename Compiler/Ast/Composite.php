<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Contracts\AstNodeInterface;

class Composite extends CompositeAstNodeAbstract
{
    protected array $items = [];

    public function __construct(
        AstNodeInterface ...$items
    ) {
        $this->items = $items;
    }

    public function components(): iterable
    {
        return $this->items;
    }
}
