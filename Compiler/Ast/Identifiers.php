<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Exceptions\ArrayEmtpyException;

class Identifiers extends CompositeAstNodeAbstract
{
    protected array $identifiers = [];

    public function __construct(
        string|AstNodeInterface ...$identifiers
    ) {
        if (empty($identifiers)) {
            throw new ArrayEmtpyException();
        }

        $this->identifiers = $identifiers;
    }

    protected function components(): iterable
    {
        $result = [];
        foreach ($this->identifiers as $item) {
            $result[] =
                $item instanceof AstNodeInterface
                    ? $item
                    : new Identifier($item);
        }
        return new Imploder(', ', ...$result);
    }
}
