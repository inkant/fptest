<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Contracts\AstNodeInterface;
use FpDbTest\Compiler\Exceptions\NotEmbeddableValueException;

class Literal extends CompositeAstNodeAbstract
{
    protected array $result = [];

    public function __construct(
        protected mixed $value
    ) {
        $this->result = $this->recognize();
    }

    protected function recognize(): array
    {
        if ($this->value instanceof AstNodeInterface) {
            return [$this->value];
        }

        if ($this->value === null) {
            return [new NullLiteral()];
        }

        if (is_string($this->value)) {
            return [new StringLiteral($this->value)];
        }

        if (is_float($this->value) || is_int($this->value)) {
            return [new NumericLiteral($this->value)];
        }

        if (is_bool($this->value)) {
            return [new BoolLiteral($this->value)];
        }

        if (is_array($this->value)
            && array_is_list($this->value)
        ) {
            return [new Literals(...$this->value)];
        }

        if (is_array($this->value)
            && !array_is_list($this->value)
        ) {
            return [new KeyValue($this->value)];
        }

        throw new NotEmbeddableValueException($this->value);
    }

    protected function components(): iterable
    {
        return $this->result;
    }
}
