<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Exceptions\CompilerException;
use FpDbTest\Compiler\Exceptions\QueryParserException;
use FpDbTest\Compiler\Parsers\CollapsableExpressionParser;
use FpDbTest\Compiler\Parsers\QueryParser;
use FpDbTest\Compiler\Parsers\QuestionMarkArrayParser;
use FpDbTest\Compiler\Parsers\QuestionMarkDigitsParser;
use FpDbTest\Compiler\Parsers\QuestionMarkNamedParser;
use FpDbTest\Compiler\Parsers\QuestionMarkNumericParser;
use FpDbTest\Compiler\Parsers\QuestionMarkPositionalParser;
use FpDbTest\Compiler\Parsers\QuestionMarkSharpParser;
use FpDbTest\Compiler\Parsers\SharpNamedParser;
use FpDbTest\Compiler\Parsers\SharpPositionalParser;

class Query extends CompositeAstNodeAbstract
{
    protected array $named      = [];
    protected array $ordered    = [];
    protected iterable $components;
    protected QueryParser $parser;

    /**
     * @throws \Exception
     */
    public function __construct(
        protected string $template,
        protected array $args
    ) {

        $this->segregate();
        $parsersArgs = [
            $this->named,
            $this->ordered
        ];

        $leafs = [
            // ?d
            new QuestionMarkDigitsParser(...$parsersArgs),
            // ?f
            new QuestionMarkNumericParser(...$parsersArgs),
            // ?a
            new QuestionMarkArrayParser(...$parsersArgs),
            // ?name
            new QuestionMarkNamedParser(...$parsersArgs),
            // ?
            new QuestionMarkPositionalParser(...$parsersArgs),
            // ?#
            new QuestionMarkSharpParser(...$parsersArgs),
            // #name
            new SharpNamedParser(...$parsersArgs),
            // #
            new SharpPositionalParser(...$parsersArgs),
        ];

        $this->parser = new QueryParser(
            // {*}
            new CollapsableExpressionParser(...$leafs),
            ...$leafs,
        );

    }

    /**
     * @throws QueryParserException
     */
    protected function components(): iterable
    {
        return $this->components ??= $this->parser->parse($this->template);
    }

    protected function segregate(): void
    {
        $this->named = [];
        $this->ordered = [];
        foreach ($this->args as $key => $value) {
            if (is_int($key)) {
                $this->ordered[] = $value;
                continue;
            }
            $this->named[$key] = $value;
        }
    }
}
