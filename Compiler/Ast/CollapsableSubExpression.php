<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class CollapsableSubExpression extends Composite
{
    public function __construct(AstNodeAbstract ...$items)
    {
        foreach ($items as $item) {
            if ($b = $item->contains(BlackHole::class)) {
                return parent::__construct($b);
            }
        }

        return parent::__construct(...$items);
    }


}
