<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

use FpDbTest\Compiler\Contracts\AstNodeInterface;

abstract class AstNodeAbstract implements AstNodeInterface
{
    /**
     * @param class-string<AstNodeInterface> $class
     * @return ?AstNodeInterface
     */
    public function contains(string $class): ?AstNodeInterface
    {
        return
            is_a($this, $class, true)
            ? $this
            : null;
    }
}
