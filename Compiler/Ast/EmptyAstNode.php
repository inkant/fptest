<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Ast;

class EmptyAstNode extends AstNodeAbstract
{
    public function compile(): string
    {
        return '';
    }
}
