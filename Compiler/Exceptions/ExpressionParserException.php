<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class ExpressionParserException extends ParserException
{
    public function __construct(string $expression, ?\Throwable $previous = null)
    {
        parent::__construct($expression, 0, $previous);
    }
}
