<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class WrongIdentifier extends CompilerException
{
    public function __construct()
    {
        parent::__construct('Only []string or string can be identifiers');
    }
}
