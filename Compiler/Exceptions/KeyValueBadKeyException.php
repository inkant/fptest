<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class KeyValueBadKeyException extends ParserException
{
    public function __construct()
    {
        parent::__construct('arrays that are not lists can have only string keys');
    }
}
