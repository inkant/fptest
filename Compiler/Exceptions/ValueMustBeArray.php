<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class ValueMustBeArray extends CompilerException
{
    public function __construct()
    {
        parent::__construct('value must be array');
    }
}
