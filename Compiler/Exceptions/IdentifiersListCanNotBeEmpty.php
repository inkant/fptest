<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class IdentifiersListCanNotBeEmpty extends CompilerException
{
    public function __construct()
    {
        parent::__construct('List of identifiers values can not be empty');
    }
}
