<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class QueryParserException extends ParserException
{
    public function __construct(string $query, int $at, ?\Throwable $previous)
    {
        parent::__construct("\"$query\" at $at", 0, $previous);
    }
}
