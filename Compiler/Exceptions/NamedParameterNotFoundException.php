<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class NamedParameterNotFoundException extends ParserException
{
    public function __construct(string $name)
    {
        parent::__construct("named parameter \"$name\" not found");
    }
}
