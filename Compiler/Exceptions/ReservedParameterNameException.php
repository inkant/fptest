<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class ReservedParameterNameException extends ParserException
{
    public function __construct(string $name)
    {
        parent::__construct("'$name' is a reserved parameter name. Parameter name can not be equal '$name'.");
    }
}
