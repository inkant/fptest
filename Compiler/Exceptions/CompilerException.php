<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class CompilerException extends \Exception
{
}
