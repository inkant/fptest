<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class ParserException extends \Exception
{
}
