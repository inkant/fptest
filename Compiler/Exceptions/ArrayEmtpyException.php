<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class ArrayEmtpyException extends CompilerException
{
    public function __construct()
    {
        parent::__construct('array must not be empty');
    }
}
