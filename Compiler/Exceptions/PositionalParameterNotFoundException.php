<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class PositionalParameterNotFoundException extends ParserException
{
    public function __construct(int $count)
    {
        parent::__construct("Not enough positional parameters, need $count, passed " . ($count - 1));
    }
}
