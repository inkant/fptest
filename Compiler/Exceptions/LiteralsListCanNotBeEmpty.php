<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class LiteralsListCanNotBeEmpty extends CompilerException
{
    public function __construct()
    {
        parent::__construct('List of literal values can not be empty');
    }
}
