<?php

declare(strict_types=1);

namespace FpDbTest\Compiler\Exceptions;

class NotEmbeddableValueException extends CompilerException
{
    public function __construct(mixed $value)
    {
        $type = gettype($value);
        if ($type === 'object') {
            $type .= ' of ' . get_class($value);
        }

        parent::__construct('unrecognized value for literal, given ' . $type);
    }
}
