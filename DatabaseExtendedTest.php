<?php

namespace FpDbTest;

use FpDbTest\Compiler\Ast\BlackHole;
use FpDbTest\Compiler\Ast\Query;

class DatabaseExtendedTest
{
    private DatabaseInterface $db;

    public function __construct(DatabaseInterface $db)
    {
        $this->db = $db;
    }

    public function testBuildQuery(): void
    {
        $q = '
    SELECT 1 
    FROM #table 
    WHERE # = ?param 
        AND ? = #column
        AND ?foo = #foo
        ';
        $data= [
            'user_id',              // первый позиционный - первая решетка
            'table' => 'users',     // #table
            'param' => 1,           // ?param
            'column' => 'surname',  // #column
            'John Doe',              // второй позиционный - первый вопрос,
            'foo' => 'bar'          // заменит ?foo на соответствующий литерал('bar'),
                            // а #foo на соответствующий идентификатор(`bar`)
        ];

        echo "\n\n";
        echo 'Basic named and positional parameters using' . "\n";
        echo 'Data: ' . var_export($data, true) . "\n";
        echo 'Raw: ' . $q . "\n";
        echo 'Result: ' . $this->db->buildQuery($q, $data);

        echo "\n\n\n";
        $q = '
    SELECT name 
    FROM users 
    {WHERE `name` = ?param}';
        $data = ['param' => [1, 2, new BlackHole()]];
        echo 'Nested black hole in array' . "\n";
        echo 'Data: ' . var_export($data, true) . "\n";
        echo 'Raw: ' . $q . "\n";
        echo 'Result: ' . $this->db->buildQuery($q, $data);

        echo "\n\n\n";
        $q = '
    SELECT name 
    FROM users 
    WHERE EXISTS( ? )
';
        $nestedQ = '
    SELECT 1 
    FROM #table
';
        $nestedData = ['table' => 'random_table'];
        $data = [new Query($nestedQ, $nestedData)];

        echo 'Subquery interpolation' . "\n";
        echo 'Parent Raw: ' . $q . "\n";
//        echo 'Data: ' . var_export($data, true) . "\n";
        echo 'Nested raw: ' . $nestedQ . "\n";
        echo 'Nested Data: ' . var_export($nestedData, true) . "\n";
        echo 'Result: ' . $this->db->buildQuery($q, $data);

        echo "\n\n\n";
        $q = 'SELECT name FROM users WHERE ? = ? AND ? = ?';
        $data = ['value1','value2','value3'];
        echo 'Not enough positional parameters' . "\n";
        echo 'Raw: ' . $q . "\n";
        echo 'Data: ' . json_encode($data) . "\n";
        try {
            $this->db->buildQuery($q, $data);
        } catch (\Exception $e) {
            echo 'Error: ' . $this->error($e);
        }

        echo "\n\n\n";
        $q = 'SELECT name FROM users WHERE #name = ? AND ? = ?';
        $data = ['value2','value3'];
        echo 'Not found parameter name' . "\n";
        echo 'Raw: ' . $q . "\n";
        echo 'Data: ' . json_encode($data) . "\n";
        try {
            $this->db->buildQuery($q, $data);
        } catch (\Exception $e) {
            echo 'Error: ' . $this->error($e);
        }

        echo "\n\n\n";
        $q = 'SELECT name FROM users WHERE #name = ? AND ? = { AND `surname` = ? }';
        $data = ['name' => 'field','value2','value3'];
        echo 'Error in a "subexpression"' . "\n";
        echo 'Raw: ' . $q . "\n";
        echo 'Data: ' . json_encode($data) . "\n";
        try {
            $this->db->buildQuery($q, $data);
        } catch (\Exception $e) {
            echo 'Error: ' . $this->error($e);
        }

        echo "\n\n\n";
        $q = 'SELECT name FROM users WHERE #';
        $data = [1];
        echo 'Identifier wrong value"' . "\n";
        echo 'Raw: ' . $q . "\n";
        echo 'Data: ' . json_encode($data) . "\n";
        try {
            $this->db->buildQuery($q, $data);
        } catch (\Exception $e) {
            echo 'Error: ' . $this->error($e);
        }

        echo "\n\n\n";
        $q = 'SELECT name FROM users WHERE ?';
        $data = [new \stdClass()];
        echo 'Wrong value"' . "\n";
        echo 'Raw: ' . $q . "\n";
        echo 'Data: ' . json_encode($data) . "\n";
        try {
            $this->db->buildQuery($q, $data);
        } catch (\Exception $e) {
            echo 'Error: ' . $this->error($e);
        }

        echo "\n";
        echo "\n";
    }


    protected function error(\Exception $e): string
    {
        $messages = [];
        while ($e instanceof \Exception) {
            $messages[] = $e->getMessage();
            $e = $e->getPrevious();
        }
        return implode(' -> ', $messages);
    }
}
